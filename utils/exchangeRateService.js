const axios = require('axios');
const Promise = require('bluebird');
const _ = require('lodash');
const Currencies = require('./currencies');

// min fetch interval in s
const MIN_INTERVAL = 60;

const fetchRate = async currency => {
  const url = `https://api.cryptonator.com/api/ticker/${currency.key}`;
  console.log(`fetch ${url}`);
  return await axios
    .get(url)
    .then(response => {
      if (response.data && response.data.success) return response.data;

      // api errors
      throw response.data;
    })
    .catch(error => {
      console.error(url);
      console.error(error);
      return null;
    });
};

const updateAllRates = async () => {
  const currencies = Currencies.getAll();

  await Promise.map(currencies, async currency => {
    // get timestamp
    const now = _.floor(+new Date() / 1000);
    const timestamp = _.get(currency, 'updatedAt');

    // skip fetching
    if (timestamp && now - timestamp < MIN_INTERVAL) return;

    const results = await fetchRate(currency);

    const data = {
      ...currency,
      updatedAt: now
    };

    if (results) data.response = results;

    // insert to database
    Currencies.set(data);
  });
};

module.exports = {
  updateAllRates
};
