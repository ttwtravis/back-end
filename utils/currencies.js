const _ = require('lodash');

// mock database
let collections = [
  { key: 'btc-usd', name: 'Bitcoin', response: null, updatedAt: null },
  { key: 'eth-usd', name: 'Ether', response: null, updatedAt: null },
  { key: 'ltc-usd', name: 'Litecoin', response: null, updatedAt: null },
  { key: 'xmr-usd', name: 'Monero', response: null, updatedAt: null },
  { key: 'xrp-usd', name: 'Ripple', response: null, updatedAt: null },
  { key: 'doge-usd', name: 'Dogecoin', response: null, updatedAt: null },
  { key: 'dash-usd', name: 'Dash', response: null, updatedAt: null },
  { key: 'maid-usd', name: 'MaidSafeeCoin', response: null, updatedAt: null },
  { key: 'lsk-usd', name: 'Lisk', response: null, updatedAt: null },
  { key: 'sjcx-usd', name: 'Storjcoin X', response: null, updatedAt: null }
];

const set = data => {
  // invalid data
  if (!data || !data.key) return;

  collections = _.map(collections, item => {
    return item.key === data.key ? data : item;
  });
};

const getAll = () => {
  return collections;
};

module.exports = {
  set,
  getAll
};
