const express = require('express');
const router = express.Router();
const exchangeRateService = require('../utils/exchangeRateService');
const Currencies = require('../utils/currencies');

router.get('/rates', async (req, res, next) => {
  await exchangeRateService.updateAllRates();
  const results = Currencies.getAll();
  res.json(results);
});

module.exports = router;
